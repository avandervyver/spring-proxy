---
@snap[west headline]
## Spring's Proxy
@snapend

@snap[south-west byline]
### Bean Scope, Proxies, Optional and Lazy
@snapend
---
### What *did* we do?
1. With what annotation can we split out our configuration file?
1. Is that annotation needed for Spring Boot? Why?
1.  Why is it possible to override beans?
1.  What annotation do we use to retrieve a bean based on its name?
1. How are bean names assigned by Spring?

Note:
1. @Import 
1. No, automatically picked up
1. Testing purposes
1. @Qualifier
1. On @Bean, just the method name. On components, class name camel-cased.
---
### What will we look at?
1. Bean Scope
1. ** Spring's Proxy **
1. Optional Beans
1. @Lazy
1. @PostConstruct 
1. @Predestroy 
---
### What is better? Method A or B?
```
@Configuration
public class ConfigurationExample {
    @Bean
    public MyBean createABean() {
        return new MyBean();
    }
    @Bean BeanDependancy OptionA() {
        return new BeanDependancy( createABean());
    }

    @Bean BeanDependancy OptionB() {
        return new BeanDependancy(new MyBean());
    }
}
```
Note:
*This code formatting is only to save space!*
Option A is better,  the Bean that will be injected will be retrieved from Spring's Application Context (This bean will have the scope 'Singleton'). Option B, on the other hand, will create its copy of the bean. 

---
### MyBean is a Singleton? Why?
```
@Configuration
public class ConfigurationExample {
    @Bean
    public myBean() {
        return new MyBean();
    }
    @Bean BeanDependancy OptionA() {
        return new BeanDependancy(myBean());
    }

    @Bean BeanDependancy OptionB() {
        return new BeanDependancy(myBean());
    }
}
```

---
### Bean Scope

** Scope == Lifetime **
The default scope for any bean is 'singleton.'
We do however have control over the scope of a bean.
How can we assign a different scope to a bean?

---
### @Scope('singleton')

* Only a single bean per application
```
@Component
@Scope("singleton")
public class SingletonScopeExample {}
```
---
### Singleton Example
```
@Autowired private ApplicationContext ac;
...
SingletonBean b1 = ac.getBean(SingletonBean.class);
SingletonBean b2 = ac.getBean(SingletonBean.class);
System.out.println("bean1 = bean2 " + (b1 == b2));
// true
```
---
### @Scope('prototype')
* Everytime a bean is requested, a new bean is created.
```
@Component
@Scope("prototype")
public class PrototypeBean {}
```
---
### Prototype Example

```
@Autowired private ApplicationContext ac;
...
PrototypeBean b1 = ac.getBean(PrototypeBean.class);
PrototypeBean b2 = ac.getBean(PrototypeBean.class);
System.out.println("bean1 = bean2 " + (b1 == b2));
// false
```
---
### Other Scopes?

  * request
  * session
  * websocket
  * custom scopes
---
* How can Spring ensure that only a single object is ever created?
---
### Spring Proxy

* One of the most essential concepts in Spring!

Note:
 * If you understand this you are pretty much a Spring expert. 

---
### Normally 
Let say two people wish to send each other a message

![Normal](/template/img/normal.png)
---
### With Proxy
![Proxy](/template/img/proxy.png)
---
### Our Application
![Our Application](/template/img/ourapp.png)
---
** Spring's Proxy mechanism the bases for AOP. **
---
### On startup
When Spring is started up, it ensures that all beans are ready for use.
* You do not want to inject a partially created bean?

---
### Optional Beans
- Does not need the bean to start up the application. 

```
@Autowired(required = false)
private Example optional1;
```
### From Java 8
```
private Optional<Example> optional2;

public OptionalExample(Optional<Example> optional2) {
  this.optional2 = optional2;
 }
```
---

### @Lazy
- The bean is created when first tried to use it.
- BAD PRACTICE: Save startup time by adding @Lazy to all beans.

---

### @Lazy Example
```
@Component
@Lazy
public class LazyExample {}
```
In Configuration file Example.
```
@Bean
@Lazy
public Example example() {
  return new Example();
}
```
Note:
 - Even if a bean is marked as Lazy, if some other bean needs it during startup the Lazy bean will be created.

---

### Circular Dependencies

** Solving the symptoms and not the problem! **
1. @Lazy
1. Setter Injection
---

### Post Construct

```
@PostConstruct
  public void runMe() {
    // Do Something
  }
```

---

### Pre Destroy

```
  @PreDestroy
  public void runMe() {
    
  }
```

```
ConfigurableApplicationContext configurableApplicationContext;
configurableApplicationContext.close();
```
---

### Always remember?
